# Projet Calculatrice

Projet réaliser en 2020, en Java.

Copyright by Marcellin Dubois <marcellin.dubois@orange.fr>


# Contenu

- Documents java:
	- Calculatrice.java
	- Fenetre.java
	- Operateurs.java
	- Bouton.java
- Document UML:
	- Calculatrice 2020.10.15 15-38-59 
- Document contenant la Javadoc:
	- Javadoc