/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatrice;

import javax.swing.JTextField;

/**
 *
 * @author siostudent
 */
public class Calculatrice {

     /**
     * Déclaration de la variable la fenêtre
     */
    private static Fenetre screen;
    /**
     * Méthode principale "main()" : Création de la fenêtre ...
     *
     *@param args : arguments éventuels transmis au programme Java
     */
    public static void main(String args[])
    {
        //Appel de la classe Fenetre
        screen = new Fenetre();
        //Fenetre java visible
        screen.setVisible(true);
    }
    
}
