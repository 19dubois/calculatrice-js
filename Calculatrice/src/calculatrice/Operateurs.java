/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 *  @author Marcellin Dubois <marcellin.dubois@orange.fr>
 */
package calculatrice;

import java.awt.FlowLayout; //permet de regler l'alignement
import java.awt.GridLayout; // manager that lays out a container's components in a rectangular grid.
import java.awt.event.ActionEvent; // event lister, definir le traitement apres une action
import java.awt.event.ActionListener; //
import javax.swing.JButton; // JButton sert en général à déclencher une action
import javax.swing.JFrame; //  classe permettant de faire une «application».
import javax.swing.JPanel; // est un conteneur élémentaire destiné à contenir d'autres composants.
import javax.swing.JTextField;
import java.awt.*;
import javax.swing.event.*;
import javax.swing.*;

/**
 *
 * Réalisation des opérations et sauvegarde de valeur
 * 
 * @author siostudent
 */
public class Operateurs extends JButton implements ActionListener{
    private Fenetre f;
    public String resultat;
    
    /**
     * 
     * Définie la class Opération
     * 
     * @param operateur: correspond à l'opérateur appuyer par l'utilisateur
     * @param a: correspond à la class Fenetre
     */
    public Operateurs(String operateur,Fenetre a){
        this.f = a;
        setText(operateur);
        addActionListener(this);
    }
    /**
     * 
     * Récupére la valeur du bouton et réalise l'action correspondant au bouton
     * 
     * @param e: Chaine de caractère qui correspond à la touche appuyer
     */
    public  void actionPerformed(ActionEvent e)
    {
        String operateur = new String (e.getActionCommand());
        if((operateur.equals("C")) || (operateur.equals("MR")) || (operateur.equals("MC")) || (operateur.equals("M+")) || (operateur.equals("M-"))){
            if(operateur.equals("C")) this.C();
            if(operateur.equals("M+")) this.MPlus();
            if(operateur.equals("M-")) this.MMoin();
            if(operateur.equals("MC")) this.MC();
            if(operateur.equals("MR")) this.MR();
        }else{
            this.Verification(operateur);
        }
    }
    
    /**
     * Remise à zéro du calcul en cours d'éxecution
     */
    public void C(){
        f.setSave("","");
        f.setZoneTexte("0");
    }
    
    /**
     * Ajout dans la mémoire de la valeur afficher à l'écran
     */
    public void MPlus(){
        f.setSaveM(f.getZoneTexte());
    }
    
    /**
     * Affichage de la valeur sauvegarder
     */
    public void MR(){
        f.setZoneTexte(f.getM());
    }
    
    /**
     * Soustraction de la valeur de l'écran avec la valeur sauvegarder.
     */
    public void MMoin(){
        double valeura = Double.parseDouble(f.getZoneTexte());
        double valeurb = Double.parseDouble(f.getM());
        double calcul = valeura - valeurb;
        this.resultat = String.valueOf(calcul) ;
        this.Afficher();
    }
    
    /**
     * Remise à zéro de la valeur sauvegarder
     */
    public void MC(){
        f.setSaveM("0");
    }
    
    /**
     * Fait la racine de la valeur de l'écran
     */
    public void Racine(){
        double calcul = Double.parseDouble(f.getZoneTexte());
        calcul = Math.sqrt(calcul);
        this.resultat = String.valueOf(calcul);
        this.Afficher();
    }
    
    /**
     * Fait apparaître la valeur de Pi à l'écran
     */
    public void Pi(){
        f.setZoneTexte("3.141592653589793238462");
    }

    /**
     * 
     * Fait la puissance de la valeur présent dans l'affichage
     * 
     * @param enregistrement :Booleen qui à pour but de dire si il y a déjà un calcul en attente(false)
     */
    public void Puissance(Boolean enregistrement){
        if(enregistrement == true){
            f.setSave("^", f.getZoneTexte());
            f.setZoneTexte("0");
        }else{
            double valeura = Double.parseDouble(f.getZoneTexte());
            double valeurb = Double.parseDouble(f.getValeur());
            double calcul = Math.pow(valeura, valeurb);
            this.resultat = String.valueOf(calcul) ;
            f.setSave("+", this.resultat);
            System.out.println(this.resultat);
        }
    }

    /**
     * Fait apparaître la valeur binaire de la valeur présent dans le tableau
     */
    public void Binaire(){
        int screen = Integer.parseInt(f.getZoneTexte());
        this.resultat = Integer.toBinaryString(screen);
        this.Afficher();
    }
    
    /**
     * 
     * Vérifie le calcul à faire et si un calcul réalisé précédemment doit être réaliser pour continué
     * 
     * @param operateur: String qui correspond à la touche saisie
     */
    public void Verification(String operateur){
        //Si l'opérateur sauvegarder est vide alors
        if(f.getValeur().equals("") && !operateur.equals("=")){
            if(operateur.equals("+")) this.Plus(true);
            if(operateur.equals("-")) this.Moin(true);
            if(operateur.equals("*")) this.Fois(true);
            if(operateur.equals("/")) this.Diviser(true);
            if(operateur.equals("B")) this.Binaire();
            if(operateur.equals("√")) this.Racine();
            if(operateur.equals("π")) this.Pi();   
            if(operateur.equals("^")) this.Puissance(true);
        }else if(operateur.equals("=")){
            if(f.getOperateur().equals("+")) this.Plus(false);
            if(f.getOperateur().equals("-")) this.Moin(false);
            if(f.getOperateur().equals("*")) this.Fois(false);
            if(f.getOperateur().equals("/")) this.Diviser(false);
            if(f.getOperateur().equals("^")) this.Puissance(false);
            this.Afficher();
        }else{
            if(f.getOperateur().equals("+")) this.Plus(false);
            if(f.getOperateur().equals("-")) this.Moin(false);
            if(f.getOperateur().equals("*")) this.Fois(false);
            if(f.getOperateur().equals("/")) this.Diviser(false);
            if(f.getOperateur().equals("^")) this.Puissance(false);
            f.setSave(operateur, this.resultat);
            f.setZoneTexte("0");
            if(operateur.equals("π")) this.Pi();
        }
    }
    
    /**
    * 
    * Réalise une addition 
    * 
    * @param enregistrement :Booleen qui à pour but de dire si il y a déjà un calcul en attente(false)
    */
    public void Plus(Boolean enregistrement){
        if(enregistrement == true){
            f.setSave("+", f.getZoneTexte());
            f.setZoneTexte("0");
        }else{
            double valeura = Double.parseDouble(f.getZoneTexte());
            double valeurb = Double.parseDouble(f.getValeur());
            double calcul = valeurb + valeura;
            this.resultat = String.valueOf(calcul) ;
            f.setSave("+", this.resultat);
            System.out.println(this.resultat);
        }
    }
    
    /**
     * 
     * Réalise une soustraction
     * 
     * @param enregistrement :Booleen qui à pour but de dire si il y a déjà un calcul en attente(false)
     */
    public void Moin(Boolean enregistrement){
        if(enregistrement == true){
            f.setSave("-", f.getZoneTexte());
            f.setZoneTexte("0");
        }else{
            double valeura = Double.parseDouble(f.getZoneTexte());
            double valeurb = Double.parseDouble(f.getValeur());
            double calcul = valeurb - valeura;
            this.resultat = String.valueOf(calcul) ;
            f.setSave("-", this.resultat);
            System.out.println(this.resultat);
        }
    }
    
    /**
     * 
     * Réalise une multiplication
     * 
     * @param enregistrement :Booleen qui à pour but de dire si il y a déjà un calcul en attente(false)
     */
    public void Fois(Boolean enregistrement){
        if(enregistrement == true){
            f.setSave("*", f.getZoneTexte());
            f.setZoneTexte("0");
        }else{
            double valeura = Double.parseDouble(f.getZoneTexte());
            double valeurb = Double.parseDouble(f.getValeur());
            double calcul = valeurb * valeura;
            this.resultat = String.valueOf(calcul) ;
            f.setSave("*", this.resultat);
            System.out.println(this.resultat);
        }
    }
    
    /**
     * 
     * Réalise une division
     * 
     * @param enregistrement :Booleen qui à pour but de dire si il y a déjà un calcul en attente(false)
     */
    public void Diviser(Boolean enregistrement){
        if(enregistrement == true){
            f.setSave("/", f.getZoneTexte());
            f.setZoneTexte("0");
        }else{
            double valeura = Double.parseDouble(f.getZoneTexte());
            double valeurb = Double.parseDouble(f.getValeur());
            double calcul = valeurb / valeura;
            this.resultat = String.valueOf(calcul) ;
            f.setSave("/", this.resultat);
            System.out.println(this.resultat);
        }
    }
    
    /**
     * Affiche le résultat final sur l'écran
     */
    public void Afficher(){
        f.setZoneTexte(this.resultat);
        f.setSave("","");
    }
}
