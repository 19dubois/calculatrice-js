/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 * @author Marcellin Dubois <marcellin.dubois@orange.fr>
 */
package calculatrice;

import java.awt.FlowLayout; //permet de regler l'alignement
import java.awt.GridLayout; // manager that lays out a container's components in a rectangular grid.
import java.awt.event.ActionEvent; // event lister, definir le traitement apres une action
import java.awt.event.ActionListener; //
import javax.swing.JButton; // JButton sert en général à déclencher une action
import javax.swing.JFrame; //  classe permettant de faire une «application».
import javax.swing.JPanel; // est un conteneur élémentaire destiné à contenir d'autres composants.
import javax.swing.JTextField;
import java.awt.*;
import javax.swing.event.*;
import javax.swing.*;
/**
 * class Fenetre
 *
 * S'occupe de l'affichage de la fenetre et de l'appel des autres class
 *
 *
 */
public class Fenetre extends JFrame{
    
    public String operateur = "";
    public String valeurpred = "";
    public String Msave = "";
    private JTextField zoneTexte; 
    
    /**
     *  Construit la class Fenetre et definie la Fenetre
     */
    public Fenetre(){
        // Titre et taille de la fenêtre ...
        setTitle("Calculatrice SIO2 2020");
        setSize(330, 425);
        Composant();
        //Comportement à la fermeture de la fenêtre ...
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    }   
    
    /**
     *  Defini le contenu de la Fenetre
     */
    public void Composant(){
        JPanel calculettePanel = new JPanel();
        calculettePanel.setPreferredSize(new Dimension(300, 80));

        JPanel clavier = new JPanel();
        clavier.setPreferredSize(new Dimension(200, 50));
        //clavier.setBackground(Color.RED);

        JPanel clavieroperator = new JPanel();
        //clavieroperator.setBackground(Color.yellow);
        clavieroperator.setPreferredSize(new Dimension(80, 80));
        
        JPanel clavierspecialoperator = new JPanel();
        //clavierspecialoperator.setBackground(Color.green);
        clavierspecialoperator.setPreferredSize(new Dimension(80, 80));
        
        calculettePanel.setLayout(new FlowLayout());
        clavier.setLayout(new GridLayout(4, 3, 2, 2)); 
        calculettePanel.setLayout(new GridLayout(1, 1, 1, 1));
        clavieroperator.setLayout(new GridLayout(5, 1, 1, 1));
        clavierspecialoperator.setLayout(new GridLayout(2, 1, 1, 1));
        
        zoneTexte = new JTextField(30);
        zoneTexte.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        zoneTexte.setEditable(false);
        zoneTexte.setText("0");
        zoneTexte.setBackground(Color.cyan);
        zoneTexte.setForeground(Color.black);
        zoneTexte.setFont(new Font("Arial", Font.PLAIN, 20));

        //clavier.setLayout(new BoxLayout(clavier, BoxLayout.Y_AXIS));
        clavier.add(clavieroperator);

        add(calculettePanel, BorderLayout.NORTH);
        add(clavier, BorderLayout.WEST);
        add(clavieroperator, BorderLayout.EAST);
        add(clavierspecialoperator, BorderLayout.SOUTH);
        
        calculettePanel.add(zoneTexte);
        
        String valeur = zoneTexte.getText();
        
        clavier.add(new Bouton("1",this));
        clavier.add(new Bouton("2",this));
        clavier.add(new Bouton("3",this));
        clavier.add(new Bouton("4",this));
        clavier.add(new Bouton("5",this));
        clavier.add(new Bouton("6",this));
        clavier.add(new Bouton("7",this));
        clavier.add(new Bouton("8",this));
        clavier.add(new Bouton("9",this));
        clavier.add(new Bouton(".",this)).setForeground(Color.RED);
        clavier.add(new Bouton("0",this));
        clavier.add(new Operateurs("=",this)).setForeground(Color.RED);
        clavieroperator.add(new Operateurs("C",this)).setForeground(Color.RED);
        clavieroperator.add(new Operateurs("+",this));
        clavieroperator.add(new Operateurs("-",this));
        clavieroperator.add(new Operateurs("*",this));
        clavieroperator.add(new Operateurs("/",this));
        
        clavierspecialoperator.add(new Operateurs("MR",this));
        clavierspecialoperator.add(new Operateurs("MC",this));
        clavierspecialoperator.add(new Operateurs("M+",this));
        clavierspecialoperator.add(new Operateurs("M-",this));
        clavierspecialoperator.add(new Operateurs("^",this));
        clavierspecialoperator.add(new Operateurs("B",this));
        clavierspecialoperator.add(new Operateurs("π",this));
        clavierspecialoperator.add(new Operateurs("√",this));
    }
    
    /**
     * 
     * Affiche dans l'écran de la calculatrice
     * 
     * @param txt: String de ce que va afficher l'écran de la calculatrice
     */
    public void setZoneTexte(String txt)
    {
        zoneTexte.setText(txt);
    }
    
    /**
     * 
     * Renvoie la valeur présent dans l'écran
     * 
     * @return String: retourne la chaine présent dans l'écran
     */
    public String getZoneTexte()
    {
        return zoneTexte.getText();
    }
    
    /**
     * 
     * Enregistre l'opérateur et la valeur du calcul en cours
     * 
     * @param operateur :chaine qui correspond à l'opérateur appuyer
     * @param valeurpred :chaine qui correspond à la valeur entré précédemment dans l'écran 
     */
    public void setSave(String operateur,String valeurpred)
    {
        this.operateur = operateur;
        this.valeurpred = valeurpred;    
    }
    
    /**
     * 
     * Renvoie l'opérateur
     * 
     * @return String: Retourne l'opérateur enregistrer
     */
    public String getOperateur()
    {
        return this.operateur; 
    }
    
    /**
     * 
     * Revoie la valeur précédente
     * 
     * @return String: Retourn la valeur enregistrer
     */
    public String getValeur()
    {
        return this.valeurpred; 
    }
    
    /**
     * 
     * Sauvegarde la valeur de l'écran voulant être sauvegarder
     * 
     * @param Valeur: chaine de caractère de la valeur de l'affichage 
     */
    public void setSaveM(String Valeur)
    {
        this.Msave=Valeur; 
    }

    /**
     * 
     * Retourne la valeur enregistrer
     * 
     * @return String: de la valeur enregistrer
     */
    public String getM()
    {
        return this.Msave; 
    }
}

