/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 *  @author Marcellin Dubois <marcellin.dubois@orange.fr>
 */
package calculatrice;

import java.awt.FlowLayout; //permet de regler l'alignement
import java.awt.GridLayout; // manager that lays out a container's components in a rectangular grid.
import java.awt.event.ActionEvent; // event lister, definir le traitement apres une action
import java.awt.event.ActionListener; //
import javax.swing.JButton; // JButton sert en général à déclencher une action
import javax.swing.JFrame; //  classe permettant de faire une «application».
import javax.swing.JPanel; // est un conteneur élémentaire destiné à contenir d'autres composants.
import javax.swing.JTextField;
import java.awt.*;
import javax.swing.event.*;
import javax.swing.*;

/**
 * class Bouton
 *
 * S'occupe de l'affichage du clavier avec les numéros
 *
 * @author Marcellin Dubois <marcellin.dubois@orange.fr>
 */
public class Bouton extends JButton implements ActionListener{
    private Fenetre f;
    
    /**
     * Definie la classe Bouton
     * 
     * @param txt: Chaine texte qui correspond à la touche appuyer 
     * @param a: Fenetre qui correspond à la class Fenetre
     */
    public Bouton(String txt, Fenetre a){
        this.f = a;
        setText(txt);
        addActionListener(this);
    }
    
    /**
     * 
     * Récupére la valeur du bouton et la concatène si néccessaire
     * 
     * @param e: Chaine de caractère qui correspond à la touche appuyer
     */
    public  void actionPerformed(ActionEvent e)
    {
        String message = new String (e.getActionCommand());
        //Verifie si le point est déjà présent dans le programme
        if(!f.getZoneTexte().contains(".")){
            if (f.getZoneTexte().equals("0")) f.setZoneTexte(message);
            else f.setZoneTexte(f.getZoneTexte() + message);
        }else if(!message.equals(".")){
            if (f.getZoneTexte().equals("0")) f.setZoneTexte(message);
            else f.setZoneTexte(f.getZoneTexte() + message);
        }   
    }
    
}
